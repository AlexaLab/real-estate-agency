﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Home.Dom
{
    public partial class FormInfoAgent : Form
    {

        int mode;
        int idAgent;

        SqlConnection connection;
        SqlCommand command;

        byte[] imgBytes;
        //Bitmap bit;
        Image bit;
        MemoryStream stream;

        public FormInfoAgent(int mode, int idAgent)
        {
            InitializeComponent();

            this.mode = mode;
            this.idAgent = idAgent;

            connection = new SqlConnection();
            connection.ConnectionString = ClassTotal.connectionString;
            connection.Open();

            command = connection.CreateCommand();
            command.CommandText = $"SELECT * FROM [Users], [Agents] WHERE [ID_Role] = 3 AND [ID_User]  = {idAgent} AND [ID_agent] = {idAgent}";

            try
            {
                SqlDataReader dataReader = command.ExecuteReader();
                dataReader.Read();

                this.textBoxUserID.Text = dataReader["ID_agent"].ToString();
                this.textBoxLogin.Text = dataReader["Login"].ToString();
                this.textBoxPass.Text = dataReader["Password"].ToString();
                this.textBoxRoleID.Text = dataReader["ID_Role"].ToString();
                this.textBoxName.Text = dataReader["User_Name"].ToString();
                this.maskedTextBoxPhone.Text = dataReader["User_Phone"].ToString();
                this.textBoxProc.Text = dataReader["Procent"].ToString();
                this.textBoxSum.Text = dataReader["Sum_Deals"].ToString();
                this.textBoxCountDeals.Text = dataReader["Count_Deals"].ToString();
                this.checkBoxStatus.Checked = (bool)dataReader["Agents_Status"];

                if (dataReader.IsDBNull(dataReader.GetOrdinal("Agent_Photo")))
                {
                    this.pictureBoxPhoto.Image = Properties.Resources.user;
                }

                else
                {
                    imgBytes = (byte[])dataReader.GetSqlBinary(dataReader.GetOrdinal("Agent_Photo"));
                    stream = new MemoryStream(imgBytes);
                    bit = Image.FromStream(stream);
                    this.pictureBoxPhoto.Image = bit;
                }

                dataReader.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                Application.Exit();
            }
        }

        public FormInfoAgent()
        {
            InitializeComponent();
            this.textBoxUserID.Text = "";
            this.textBoxLogin.Text = "";
            this.textBoxPass.Text = "";
            this.textBoxRoleID.Text = "3";
            this.textBoxName.Text = "";
            this.maskedTextBoxPhone.Text = "";
            this.textBoxProc.Text = "3,00";
            this.textBoxSum.Text = "0";
            this.textBoxCountDeals.Text = "0";
            this.checkBoxStatus.Checked = true;
            imgBytes = null;
            this.textBoxUserID.Enabled = this.textBoxSum.Enabled = this.textBoxCountDeals.Enabled = false;
            mode = 3;
        }

        private void FormInfoAgent_Load(object sender, EventArgs e)
        {
            if (this.mode == 1)
            {
                panel1.Enabled = false;
                buttonExec.Enabled = false;
            }

            else if (this.mode == 2)
            {
                panel1.Enabled = true;
                buttonExec.Enabled = true;

                textBoxUserID.Enabled = false;
                textBoxLogin.Enabled = false;
                textBoxSum.Enabled = false;
                textBoxCountDeals.Enabled = false;
            }

            else if (mode == 3)
            {
                panel1.Enabled = true;
                buttonExec.Enabled = true;
            }
        }

        private void buttonExec_Click(object sender, EventArgs e)
        {
            if (this.mode == 2)
            {
                command = connection.CreateCommand();

                if (imgBytes == null)
                {
                    command.CommandText = $"UPDATE [Agents] SET [Agent_Photo] = NULL, [Procent] = {Convert.ToDouble(textBoxProc.Text)}, [Agents_Status] = '{checkBoxStatus.Checked.ToString()}' WHERE [ID_agent] = {idAgent}";
                }

                else
                {
                    command.CommandText = $"UPDATE [Agents] SET [Agent_Photo] = @imgBytes, [Procent] = {Convert.ToDouble(textBoxProc.Text)}, [Agents_Status] = '{checkBoxStatus.Checked.ToString()}' WHERE [ID_agent] = {idAgent}";
                    command.Parameters.AddWithValue("@imgBytes", imgBytes);
                }
                command.ExecuteNonQuery();

                command.CommandText = $"UPDATE [Users] SET [User_Name] = '{textBoxName.Text}', [User_Phone] = '{maskedTextBoxPhone.Text}' WHERE [ID_User] = {idAgent}";
                //command.ExecuteNonQuery();
            }

            else if (mode == 3)
            {
                connection = new SqlConnection();
                connection.ConnectionString = ClassTotal.connectionString;
                connection.Open();
                command = connection.CreateCommand();
                string log = this.textBoxLogin.Text;
                string pas = this.textBoxPass.Text;
                string FIO = this.textBoxName.Text;
                string tel = this.maskedTextBoxPhone.Text;
                double proc = double.Parse(this.textBoxProc.Text);
                bool status = this.checkBoxStatus.Checked;
                command.Parameters.Clear();

                try
                {
                    command.Parameters.Clear();
                    command.CommandText = "SearchLogPas";
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@log", log);
                    command.Parameters.AddWithValue("@Pas", pas);
                    SqlDataReader dr = command.ExecuteReader();
                    if (dr.Read())
                    {
                        MessageBox.Show("Такие данные уже есть в системе.\r\n Введите другие данные");
                        return;
                    }

                    dr.Close();
                    command.Parameters.Clear();
                    command.CommandText = "INSERT INTO[Users] VALUES(@log, @pas, 3, @FIO, @tel)";
                    command.Parameters.AddWithValue("@log", log);
                    command.Parameters.AddWithValue("@Pas", pas);
                    command.Parameters.AddWithValue("@FIO", FIO);
                    command.Parameters.AddWithValue("@tel", tel);
                    command.CommandText = command.CommandText;
                    command.CommandType = CommandType.Text;
                    command.ExecuteNonQuery();
                    command.CommandText = "SELECT MAX(ID_user) FROM[Users]";
                    command.CommandText = command.CommandText;
                    int idAgent = (int)command.ExecuteScalar();

                    if (imgBytes == null)
                    {
                        command.CommandText = "INSERT INTO[Agents] VALUES(@idAgent, @proc, 0.00, 0, 1, null)";
                        command.Parameters.AddWithValue("@proc", proc);
                        command.Parameters.AddWithValue("@idAgent", idAgent);
                    }

                    else
                    {
                        command.CommandText = "INSERT INTO[Agents] VALUES(@idAgent, @proc, 0.00, 0, 1, @img)";
                        command.Parameters.AddWithValue("@proc", proc);
                        command.Parameters.AddWithValue("@idAgent", idAgent);
                        command.Parameters.AddWithValue("@img", imgBytes);
                    }
                    command.CommandText = command.CommandText;
                    command.ExecuteNonQuery();
                    MessageBox.Show("Новый агент удачно добавлен в систему");
                    this.Close();
                }
                catch (SqlException ex)
                {
                    MessageBox.Show("Не удалось добавить новые данные");
                    MessageBox.Show(ex.Message);
                    return;
                }
            }
            this.Close();
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonSelPhoto_Click(object sender, EventArgs e)
        {
            OpenFileDialog openDialog = new OpenFileDialog(); //создание диалогового окна для выбора файла
            openDialog.Filter = "Image Files(*.BMP;*.JPG;*.PNG)|*.BMP;*.JPG;*.PNG|All files (*.*)|*.*"; //формат загружаемого файла

            if (openDialog.ShowDialog() == DialogResult.OK) //если в окне была нажата кнопка "ОК"
            {
                try
                {
                    bit = new Bitmap(openDialog.FileName);
                    pictureBoxPhoto.Image = bit;
                    pictureBoxPhoto.Invalidate();

                    stream = new MemoryStream();
                    bit.Save(stream, System.Drawing.Imaging.ImageFormat.Png);
                    imgBytes = stream.ToArray();
                }
                catch
                {
                    DialogResult rezult = MessageBox.Show("Невозможно открыть выбранный файл",
                    "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
    }
}
