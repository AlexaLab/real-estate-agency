﻿namespace Home.Dom
{
    partial class FormInfoClient
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonBackClient = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.textBoxRegistrationClient = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.maskedTextBoxPhoneClient = new System.Windows.Forms.MaskedTextBox();
            this.textBoxBirthdayClient = new System.Windows.Forms.TextBox();
            this.textBoxEmailClient = new System.Windows.Forms.TextBox();
            this.textBoxAddressClient = new System.Windows.Forms.TextBox();
            this.textBoxNameClient = new System.Windows.Forms.TextBox();
            this.textBoxSnilsClient = new System.Windows.Forms.TextBox();
            this.textBoxInnClient = new System.Windows.Forms.TextBox();
            this.textBoxPassportClient = new System.Windows.Forms.TextBox();
            this.textBoxIDClient = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.buttonSaveClient = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonBackClient
            // 
            this.buttonBackClient.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.buttonBackClient.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonBackClient.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonBackClient.Location = new System.Drawing.Point(434, 279);
            this.buttonBackClient.Name = "buttonBackClient";
            this.buttonBackClient.Size = new System.Drawing.Size(281, 62);
            this.buttonBackClient.TabIndex = 28;
            this.buttonBackClient.Text = "Вернуться к списку клиентов";
            this.buttonBackClient.UseVisualStyleBackColor = false;
            this.buttonBackClient.Click += new System.EventHandler(this.buttonBackClient_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.textBoxRegistrationClient);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.maskedTextBoxPhoneClient);
            this.panel1.Controls.Add(this.textBoxBirthdayClient);
            this.panel1.Controls.Add(this.textBoxEmailClient);
            this.panel1.Controls.Add(this.textBoxAddressClient);
            this.panel1.Controls.Add(this.textBoxNameClient);
            this.panel1.Controls.Add(this.textBoxSnilsClient);
            this.panel1.Controls.Add(this.textBoxInnClient);
            this.panel1.Controls.Add(this.textBoxPassportClient);
            this.panel1.Controls.Add(this.textBoxIDClient);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(13, 14);
            this.panel1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(702, 257);
            this.panel1.TabIndex = 26;
            // 
            // textBoxRegistrationClient
            // 
            this.textBoxRegistrationClient.Location = new System.Drawing.Point(416, 161);
            this.textBoxRegistrationClient.Name = "textBoxRegistrationClient";
            this.textBoxRegistrationClient.Size = new System.Drawing.Size(186, 20);
            this.textBoxRegistrationClient.TabIndex = 26;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(310, 164);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(100, 13);
            this.label9.TabIndex = 25;
            this.label9.Text = "Дата регистрации";
            // 
            // maskedTextBoxPhoneClient
            // 
            this.maskedTextBoxPhoneClient.Location = new System.Drawing.Point(88, 84);
            this.maskedTextBoxPhoneClient.Mask = "+7 (999) 000-00-00";
            this.maskedTextBoxPhoneClient.Name = "maskedTextBoxPhoneClient";
            this.maskedTextBoxPhoneClient.Size = new System.Drawing.Size(200, 20);
            this.maskedTextBoxPhoneClient.TabIndex = 24;
            // 
            // textBoxBirthdayClient
            // 
            this.textBoxBirthdayClient.Location = new System.Drawing.Point(402, 124);
            this.textBoxBirthdayClient.Name = "textBoxBirthdayClient";
            this.textBoxBirthdayClient.Size = new System.Drawing.Size(200, 20);
            this.textBoxBirthdayClient.TabIndex = 19;
            // 
            // textBoxEmailClient
            // 
            this.textBoxEmailClient.Location = new System.Drawing.Point(88, 117);
            this.textBoxEmailClient.Name = "textBoxEmailClient";
            this.textBoxEmailClient.Size = new System.Drawing.Size(200, 20);
            this.textBoxEmailClient.TabIndex = 18;
            // 
            // textBoxAddressClient
            // 
            this.textBoxAddressClient.Location = new System.Drawing.Point(88, 145);
            this.textBoxAddressClient.Multiline = true;
            this.textBoxAddressClient.Name = "textBoxAddressClient";
            this.textBoxAddressClient.Size = new System.Drawing.Size(200, 70);
            this.textBoxAddressClient.TabIndex = 17;
            // 
            // textBoxNameClient
            // 
            this.textBoxNameClient.Location = new System.Drawing.Point(88, 52);
            this.textBoxNameClient.Name = "textBoxNameClient";
            this.textBoxNameClient.Size = new System.Drawing.Size(200, 20);
            this.textBoxNameClient.TabIndex = 15;
            // 
            // textBoxSnilsClient
            // 
            this.textBoxSnilsClient.Location = new System.Drawing.Point(377, 87);
            this.textBoxSnilsClient.Name = "textBoxSnilsClient";
            this.textBoxSnilsClient.Size = new System.Drawing.Size(200, 20);
            this.textBoxSnilsClient.TabIndex = 14;
            // 
            // textBoxInnClient
            // 
            this.textBoxInnClient.Location = new System.Drawing.Point(377, 54);
            this.textBoxInnClient.Name = "textBoxInnClient";
            this.textBoxInnClient.Size = new System.Drawing.Size(200, 20);
            this.textBoxInnClient.TabIndex = 13;
            // 
            // textBoxPassportClient
            // 
            this.textBoxPassportClient.Location = new System.Drawing.Point(377, 22);
            this.textBoxPassportClient.Name = "textBoxPassportClient";
            this.textBoxPassportClient.Size = new System.Drawing.Size(200, 20);
            this.textBoxPassportClient.TabIndex = 12;
            // 
            // textBoxIDClient
            // 
            this.textBoxIDClient.Location = new System.Drawing.Point(99, 8);
            this.textBoxIDClient.Name = "textBoxIDClient";
            this.textBoxIDClient.Size = new System.Drawing.Size(50, 20);
            this.textBoxIDClient.TabIndex = 11;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(310, 124);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(86, 13);
            this.label11.TabIndex = 10;
            this.label11.Text = "Дата рождения";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(20, 117);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(32, 13);
            this.label8.TabIndex = 7;
            this.label8.Text = "Email";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(20, 145);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(38, 13);
            this.label7.TabIndex = 6;
            this.label7.Text = "Адрес";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(20, 87);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(52, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "Телефон";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(20, 58);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(34, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "ФИО";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(321, 87);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(45, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "СНИЛС";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(321, 54);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(31, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "ИНН";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(321, 25);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(50, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Паспорт";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(85, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Номер клиента";
            // 
            // buttonSaveClient
            // 
            this.buttonSaveClient.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.buttonSaveClient.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSaveClient.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonSaveClient.Location = new System.Drawing.Point(13, 278);
            this.buttonSaveClient.Name = "buttonSaveClient";
            this.buttonSaveClient.Size = new System.Drawing.Size(281, 62);
            this.buttonSaveClient.TabIndex = 29;
            this.buttonSaveClient.Text = "Сохранить";
            this.buttonSaveClient.UseVisualStyleBackColor = false;
            this.buttonSaveClient.Click += new System.EventHandler(this.buttonSaveClient_Click);
            // 
            // FormInfoClient
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(724, 350);
            this.Controls.Add(this.buttonSaveClient);
            this.Controls.Add(this.buttonBackClient);
            this.Controls.Add(this.panel1);
            this.Name = "FormInfoClient";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.FormInfoClient_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonBackClient;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.MaskedTextBox maskedTextBoxPhoneClient;
        private System.Windows.Forms.TextBox textBoxBirthdayClient;
        private System.Windows.Forms.TextBox textBoxEmailClient;
        private System.Windows.Forms.TextBox textBoxAddressClient;
        private System.Windows.Forms.TextBox textBoxNameClient;
        private System.Windows.Forms.TextBox textBoxSnilsClient;
        private System.Windows.Forms.TextBox textBoxInnClient;
        private System.Windows.Forms.TextBox textBoxPassportClient;
        private System.Windows.Forms.TextBox textBoxIDClient;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxRegistrationClient;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button buttonSaveClient;
    }
}