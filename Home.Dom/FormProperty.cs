﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Home.Dom
{
    public partial class FormProperty : Form
    {
        public FormProperty()
        {
            InitializeComponent();
        }

        SqlConnection connection = new SqlConnection();

        private void FormProperty_Load(object sender, EventArgs e)
        {
            buttonReset_Click(null, null);
            String SQLStringRegion = "SELECT * FROM [Regions]";
            SqlDataAdapter adapterRegion = new SqlDataAdapter(SQLStringRegion, ClassTotal.connectionString);
            DataTable dtReg = new DataTable();
            adapterRegion.Fill(dtReg);
            comboBoxRegion.DataSource = dtReg;
            comboBoxRegion.DisplayMember = "Name_region";
            comboBoxRegion.ValueMember = "ID_region";

            TableUpdate(0, 0, 0, 1000000000);
        }

        public void TableUpdate(int reg, int room, double costMin, double costMax)
        {
            connection.ConnectionString = ClassTotal.connectionString;
            connection.Open();
            SqlCommand command = new SqlCommand();
            command.CommandText = "InfoONFilters";
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.Clear();
            command.Parameters.AddWithValue("@IDAgent", ClassTotal.ID_User);
            command.Parameters.AddWithValue("@IDRegion", reg);
            command.Parameters.AddWithValue("@Room", room);
            command.Parameters.AddWithValue("@CostMin", costMin);
            command.Parameters.AddWithValue("@CostMax", costMax);
            command.Connection = connection;    
            SqlDataReader dataReader = command.ExecuteReader();

            int i = -1;

            dataGridViewObjects.RowCount = 0;

            while (dataReader.Read())
            {
                i++;
                dataGridViewObjects.Rows.Add();
                dataGridViewObjects[0, i].Value = (string)dataReader["Name_client"];
                dataGridViewObjects[1, i].Value = (decimal)dataReader["Cost_object"];
                dataGridViewObjects[2, i].Value = (string)dataReader["Name_region"];
                dataGridViewObjects[3, i].Value = (string)dataReader["Address_object"];
                dataGridViewObjects[4, i].Value = (int)dataReader["Rooms"];
                dataGridViewObjects[5, i].Value = (decimal)dataReader["Square"];
                dataGridViewObjects[6, i].Value = dataReader["Date_object"];
            }
            dataReader.Close();
            connection.Close();
        }

        private void buttonReset_Click(object sender, EventArgs e)
        {

        }

        private void buttonInfoOwner_Click(object sender, EventArgs e)
        {

        }

        private void buttonBack_Click(object sender, EventArgs e)
        {
            this.Close(); 
        }

        private void buttonAddON_Click(object sender, EventArgs e)
        {

        }

        private void buttonSearch_Click(object sender, EventArgs e)
        {
            int reg = 0, room = 0;
            double costMin = 0, costMax = 100000000;
            
            if (checkBoxRegion.Checked)
            {
                reg = (int)comboBoxRegion.SelectedValue;
            }

            if (checkBoxRoom.Checked)
            {
                room = (int)numericUpDownRoomAmount.Value;
            }

            if (checkBoxPrice.Checked)
            {
                costMin = Convert.ToDouble(textBoxPriceFrom.Text);
                costMax = Convert.ToDouble(textBoxPriceTo.Text);
            }

            TableUpdate(reg, room, costMin, costMax);
        }

        private void textBoxPriceFrom_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsNumber(e.KeyChar) | (Char.IsPunctuation(e.KeyChar)) | e.KeyChar == '\b') return;
            else
                e.Handled = true;
        }
    }
}
