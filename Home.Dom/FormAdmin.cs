﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Home.Dom
{
    public partial class FormAdmin : Form
    {
        public FormAdmin()
        {
            InitializeComponent();
        }

        int idAgent = 0;
        int mode = 0;
        SqlConnection connection = new SqlConnection();

        private void buttonBack_Click(object sender, EventArgs e)
        {
            this.Close();            
        }

        private void FormAdmin_Load(object sender, EventArgs e)
        {
            TableUpdate();
        }

        private void buttonMore_Click(object sender, EventArgs e)
        {
            mode = 1;
            idAgent = (int)this.dataGridViewAgents.CurrentRow.Cells[0].Value;

            FormInfoAgent formInfoAgent = new FormInfoAgent(mode, idAgent);
            this.Hide();
            formInfoAgent.ShowDialog();
            this.Show();
        }

        private void buttonUpdate_Click(object sender, EventArgs e)
        {
            mode = 2;
            idAgent = (int)this.dataGridViewAgents.CurrentRow.Cells[0].Value;

            FormInfoAgent formInfoAgent = new FormInfoAgent(mode, idAgent);
            this.Hide();
            formInfoAgent.ShowDialog();
            this.Show();
            TableUpdate();
        }


        private void buttonAdd_Click(object sender, EventArgs e)
        {
            mode = 3;
            FormInfoAgent formInfoAgent = new FormInfoAgent();
            this.Hide();
            formInfoAgent.ShowDialog();
            this.Show();
            TableUpdate();
        }

        public void TableUpdate()
        {
            connection.ConnectionString = ClassTotal.connectionString;
            connection.Open();

            SqlCommand command = new SqlCommand();
            command.CommandText = $"SELECT * FROM [Users], [Agents] WHERE [ID_Role] = 3 AND [ID_User]  = [ID_agent]";
            command.Connection = connection;
            SqlDataReader dataReader = command.ExecuteReader();

            this.dataGridViewAgents.RowCount = 0;
            int i = -1;

            while (dataReader.Read())
            {
                i++;
                dataGridViewAgents.Rows.Add();
                dataGridViewAgents[0, i].Value = (int)dataReader["ID_User"];
                dataGridViewAgents[1, i].Value = (string)dataReader["Login"];
                dataGridViewAgents[2, i].Value = (string)dataReader["Password"];
                dataGridViewAgents[3, i].Value = (string)dataReader["User_Name"];
                dataGridViewAgents[4, i].Value = (string)dataReader["User_Phone"];
                dataGridViewAgents[5, i].Value = (bool)dataReader["Agents_Status"];
            }
            dataReader.Close();
            connection.Close();
        }
    }
}
