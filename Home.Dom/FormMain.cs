﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Home.Dom
{
    public partial class FormMain : Form
    {
        public FormMain()
        {
            InitializeComponent();
        }

        private void buttonAutorize_Click(object sender, EventArgs e)
        {
            FormAutorize newForm = new FormAutorize();
            this.Hide();
            newForm.ShowDialog();
            this.Show();
        }

        private void buttonExit_Click(object sender, EventArgs e)
        {
            Environment.Exit(1);
        }

        private void Form1_Load(object sender, EventArgs e)
        {

            SqlConnection connection = new SqlConnection();
            connection.ConnectionString = ClassTotal.connectionString;

            try
            {
                connection.Open();
                MessageBox.Show("Связь с сервером установлена");
            }
            catch (SqlException ex)
            {

                switch (ex.Number)
                {
                    case 17: MessageBox.Show("Неверное имя сервера"); break;
                    case 4060: MessageBox.Show("Неверное имя базы данных"); break;
                    case 18456: MessageBox.Show("Неверное имя пользователя или пароль"); break;
                }
                MessageBox.Show(ex.Message + "Уровень ошибки: " + ex.Class); Application.Exit();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ошибка подключения: " + ex.Message); Application.Exit();
            }
            finally
            {

                if (connection.State == ConnectionState.Open) connection.Close();
            }
        }

        private void buttonAboutObject_Click(object sender, EventArgs e)
        {

        }

        private void buttonAboutAgency_Click(object sender, EventArgs e)
        {

        }
    }
}
