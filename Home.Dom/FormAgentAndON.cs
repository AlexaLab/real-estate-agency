﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Home.Dom
{
    public partial class FormAgentAndON : Form
    {
        public FormAgentAndON()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
            FormAgent fag = new FormAgent();
            fag.ShowDialog();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
            FormProperty property = new FormProperty();
            property.ShowDialog();
        }
    }
}
