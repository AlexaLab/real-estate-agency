﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Net.Mail;
using System.Net;

namespace Home.Dom
{
    public partial class FormAgent : Form
    {
        public FormAgent()
        {
            InitializeComponent();
        }

        int idAgent1 = 0;
        int mode1 = 0;
        SqlConnection connection = new SqlConnection();
        SqlCommand command = new SqlCommand();
        DataTable dt = new DataTable();

        private void FormAgent_Load(object sender, EventArgs e)
        {
            TableUpdate();
        }

        private void buttonBack_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public void TableUpdate()
        {
            connection.ConnectionString = ClassTotal.connectionString;
            connection.Open();
            command = new SqlCommand();
            command.CommandText = $"SELECT * FROM [Clients] WHERE ID_agent = {ClassTotal.ID_User}";
            command.Connection = connection;

            SqlDataReader dataReader = command.ExecuteReader();

            this.dataGridViewClients.RowCount = 0;
            int i = -1;


            while (dataReader.Read())
            {
                i++;
                dataGridViewClients.Rows.Add();
                dataGridViewClients[0, i].Value = (int)dataReader["ID_client"];
                dataGridViewClients[1, i].Value = (string)dataReader["Name_client"];
                dataGridViewClients[2, i].Value = (string)dataReader["Email"];
                dataGridViewClients[3, i].Value = (string)dataReader["Phone_client"];
                dataGridViewClients[4, i].Value = dataReader["Date_registration"];
            }
            dataReader.Close();
            connection.Close();
        }

        private void buttonMore_Click(object sender, EventArgs e)
        {
            mode1 = 1;
            idAgent1 = (int)this.dataGridViewClients.CurrentRow.Cells[0].Value;
            FormInfoClient FormInfoClient = new FormInfoClient(mode1, idAgent1);
            this.Hide();
            FormInfoClient.ShowDialog();
            this.Show();
        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            mode1 = 3;
            FormInfoClient FormInfoClient = new FormInfoClient();
            this.Hide();
            FormInfoClient.ShowDialog();
            this.Show();
            TableUpdate();
        }

        private void buttonDel_Click(object sender, EventArgs e)
        {
            command.Connection = connection;
            connection.Open();
            SqlDataAdapter adapter = new SqlDataAdapter("SELECT * FROM [Clients]", ClassTotal.connectionString);
            int delClient = (int)dataGridViewClients.CurrentRow.Cells[0].Value;
            string sqlText = "DELETE FROM [Clients] WHERE [ID_client] = @delClient";
            command.CommandText = sqlText;
            command.Parameters.AddWithValue("@delClient", delClient);
            adapter.DeleteCommand = command;
            int res = adapter.DeleteCommand.ExecuteNonQuery();
            connection.Close();
            if (res == 0)
                MessageBox.Show("Запись не удалена");
            else
            {
                adapter.Update(dt);
                MessageBox.Show("Запись удалена");
                TableUpdate();
            }
        }

        private void buttonUpdate_Click(object sender, EventArgs e)
        {
            mode1 = 2;
            idAgent1 = (int)this.dataGridViewClients.CurrentRow.Cells[0].Value;
            FormInfoClient FormInfoClient = new FormInfoClient(mode1, idAgent1);
            this.Hide();
            FormInfoClient.ShowDialog();
            this.Show();
            TableUpdate();
        }

        private void buttonSend_Click(object sender, EventArgs e)
        {
            SmtpClient smtp = new SmtpClient
            {
                Port = 587,
                Host = "smtp.gmail.com",
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(textBoxSend.Text, textBoxPass.Text)
            };

            using (MailMessage mailMessage = new MailMessage(textBoxSend.Text, dataGridViewClients.CurrentRow.Cells[2].Value.ToString()))
            {
                mailMessage.Subject = textBoxTheme.Text;
                mailMessage.Body = textBoxMessage.Text;
                foreach (string resource in listBoxAttachments.Items)
                {
                    mailMessage.Attachments.Add(new Attachment(resource));
                }

                try
                {
                    smtp.Send(mailMessage);
                    MessageBox.Show("Готово");
                }
                catch (SmtpException ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void buttonAddFile_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.ShowDialog();
            string FileName = dialog.FileName;
            listBoxAttachments.Items.Add(FileName);
        }

        private void buttonDeleteFile_Click(object sender, EventArgs e)
        {
            if (listBoxAttachments.SelectedIndex != -1)
                listBoxAttachments.Items.RemoveAt(listBoxAttachments.SelectedIndex);
            else
                MessageBox.Show("выберите элемент");
        }

        private void buttonDeleteList_Click(object sender, EventArgs e)
        {
            if (listBoxAttachments.SelectedIndex != -1)
                listBoxAttachments.Items.Clear();
            else
                MessageBox.Show("Список пустой");
        }
    }
}
