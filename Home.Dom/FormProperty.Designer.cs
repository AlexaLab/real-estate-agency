﻿namespace Home.Dom
{
    partial class FormProperty
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormProperty));
            this.buttonBack = new System.Windows.Forms.Button();
            this.buttonSearch = new System.Windows.Forms.Button();
            this.buttonReset = new System.Windows.Forms.Button();
            this.buttonInfoOwner = new System.Windows.Forms.Button();
            this.buttonInfoON = new System.Windows.Forms.Button();
            this.buttonEditON = new System.Windows.Forms.Button();
            this.buttonAddON = new System.Windows.Forms.Button();
            this.dataGridViewObjects = new System.Windows.Forms.DataGridView();
            this.Владелец = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.textBoxPriceFrom = new System.Windows.Forms.TextBox();
            this.textBoxPriceTo = new System.Windows.Forms.TextBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.checkBoxRegion = new System.Windows.Forms.CheckBox();
            this.checkBoxPrice = new System.Windows.Forms.CheckBox();
            this.checkBoxRoom = new System.Windows.Forms.CheckBox();
            this.comboBoxRegion = new System.Windows.Forms.ComboBox();
            this.numericUpDownRoomAmount = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewObjects)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownRoomAmount)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonBack
            // 
            this.buttonBack.Location = new System.Drawing.Point(528, 12);
            this.buttonBack.Name = "buttonBack";
            this.buttonBack.Size = new System.Drawing.Size(157, 42);
            this.buttonBack.TabIndex = 0;
            this.buttonBack.Text = "Возврат в меню агента";
            this.buttonBack.UseVisualStyleBackColor = true;
            this.buttonBack.Click += new System.EventHandler(this.buttonBack_Click);
            // 
            // buttonSearch
            // 
            this.buttonSearch.Location = new System.Drawing.Point(529, 72);
            this.buttonSearch.Name = "buttonSearch";
            this.buttonSearch.Size = new System.Drawing.Size(153, 23);
            this.buttonSearch.TabIndex = 1;
            this.buttonSearch.Text = "Поиск";
            this.buttonSearch.UseVisualStyleBackColor = true;
            this.buttonSearch.Click += new System.EventHandler(this.buttonSearch_Click);
            // 
            // buttonReset
            // 
            this.buttonReset.Location = new System.Drawing.Point(529, 100);
            this.buttonReset.Name = "buttonReset";
            this.buttonReset.Size = new System.Drawing.Size(153, 23);
            this.buttonReset.TabIndex = 2;
            this.buttonReset.Text = "Сброс фильтра";
            this.buttonReset.UseVisualStyleBackColor = true;
            this.buttonReset.Click += new System.EventHandler(this.buttonReset_Click);
            // 
            // buttonInfoOwner
            // 
            this.buttonInfoOwner.Location = new System.Drawing.Point(12, 394);
            this.buttonInfoOwner.Name = "buttonInfoOwner";
            this.buttonInfoOwner.Size = new System.Drawing.Size(153, 44);
            this.buttonInfoOwner.TabIndex = 3;
            this.buttonInfoOwner.Text = "Информация о владельце";
            this.buttonInfoOwner.UseVisualStyleBackColor = true;
            this.buttonInfoOwner.Click += new System.EventHandler(this.buttonInfoOwner_Click);
            // 
            // buttonInfoON
            // 
            this.buttonInfoON.Location = new System.Drawing.Point(187, 394);
            this.buttonInfoON.Name = "buttonInfoON";
            this.buttonInfoON.Size = new System.Drawing.Size(153, 44);
            this.buttonInfoON.TabIndex = 4;
            this.buttonInfoON.Text = "Информация об ОН";
            this.buttonInfoON.UseVisualStyleBackColor = true;
            // 
            // buttonEditON
            // 
            this.buttonEditON.Location = new System.Drawing.Point(359, 394);
            this.buttonEditON.Name = "buttonEditON";
            this.buttonEditON.Size = new System.Drawing.Size(153, 44);
            this.buttonEditON.TabIndex = 5;
            this.buttonEditON.Text = "Редактировать ОН";
            this.buttonEditON.UseVisualStyleBackColor = true;
            // 
            // buttonAddON
            // 
            this.buttonAddON.Location = new System.Drawing.Point(529, 394);
            this.buttonAddON.Name = "buttonAddON";
            this.buttonAddON.Size = new System.Drawing.Size(153, 44);
            this.buttonAddON.TabIndex = 6;
            this.buttonAddON.Text = "Добавить ОН";
            this.buttonAddON.UseVisualStyleBackColor = true;
            this.buttonAddON.Click += new System.EventHandler(this.buttonAddON_Click);
            // 
            // dataGridViewObjects
            // 
            this.dataGridViewObjects.AllowUserToAddRows = false;
            this.dataGridViewObjects.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewObjects.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Владелец,
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4,
            this.Column5,
            this.Column6});
            this.dataGridViewObjects.Location = new System.Drawing.Point(12, 126);
            this.dataGridViewObjects.Name = "dataGridViewObjects";
            this.dataGridViewObjects.RowHeadersVisible = false;
            this.dataGridViewObjects.Size = new System.Drawing.Size(724, 259);
            this.dataGridViewObjects.TabIndex = 7;
            // 
            // Владелец
            // 
            this.Владелец.HeaderText = "Владелец";
            this.Владелец.Name = "Владелец";
            // 
            // Column1
            // 
            this.Column1.HeaderText = "Цена";
            this.Column1.Name = "Column1";
            // 
            // Column2
            // 
            this.Column2.HeaderText = "Район";
            this.Column2.Name = "Column2";
            // 
            // Column3
            // 
            this.Column3.HeaderText = "Адрес";
            this.Column3.Name = "Column3";
            this.Column3.Width = 120;
            // 
            // Column4
            // 
            this.Column4.HeaderText = "Комнат";
            this.Column4.Name = "Column4";
            // 
            // Column5
            // 
            this.Column5.HeaderText = "Площадь";
            this.Column5.Name = "Column5";
            // 
            // Column6
            // 
            dataGridViewCellStyle8.Format = "d";
            dataGridViewCellStyle8.NullValue = null;
            this.Column6.DefaultCellStyle = dataGridViewCellStyle8;
            this.Column6.HeaderText = "Дата регистрации";
            this.Column6.Name = "Column6";
            // 
            // textBoxPriceFrom
            // 
            this.textBoxPriceFrom.Location = new System.Drawing.Point(193, 97);
            this.textBoxPriceFrom.Name = "textBoxPriceFrom";
            this.textBoxPriceFrom.Size = new System.Drawing.Size(100, 20);
            this.textBoxPriceFrom.TabIndex = 8;
            this.textBoxPriceFrom.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxPriceFrom_KeyPress);
            // 
            // textBoxPriceTo
            // 
            this.textBoxPriceTo.Location = new System.Drawing.Point(324, 97);
            this.textBoxPriceTo.Name = "textBoxPriceTo";
            this.textBoxPriceTo.Size = new System.Drawing.Size(100, 20);
            this.textBoxPriceTo.TabIndex = 9;
            this.textBoxPriceTo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxPriceFrom_KeyPress);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.BackgroundImage = global::Home.Dom.Properties.Resources.Logo;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox1.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.InitialImage")));
            this.pictureBox1.Location = new System.Drawing.Point(12, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(62, 62);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 10;
            this.pictureBox1.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(80, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(417, 22);
            this.label1.TabIndex = 11;
            this.label1.Text = "Список объектов недвижимости данного агента";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(84, 48);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(169, 17);
            this.label2.TabIndex = 12;
            this.label2.Text = "Параметры фильтрации";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(298, 100);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(21, 15);
            this.label4.TabIndex = 14;
            this.label4.Text = "до";
            // 
            // checkBoxRegion
            // 
            this.checkBoxRegion.AutoSize = true;
            this.checkBoxRegion.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.checkBoxRegion.Location = new System.Drawing.Point(85, 74);
            this.checkBoxRegion.Name = "checkBoxRegion";
            this.checkBoxRegion.Size = new System.Drawing.Size(62, 19);
            this.checkBoxRegion.TabIndex = 15;
            this.checkBoxRegion.Text = "Район";
            this.checkBoxRegion.UseVisualStyleBackColor = true;
            // 
            // checkBoxPrice
            // 
            this.checkBoxPrice.AutoSize = true;
            this.checkBoxPrice.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.checkBoxPrice.Location = new System.Drawing.Point(85, 99);
            this.checkBoxPrice.Name = "checkBoxPrice";
            this.checkBoxPrice.Size = new System.Drawing.Size(108, 19);
            this.checkBoxPrice.TabIndex = 16;
            this.checkBoxPrice.Text = "Стоимость от";
            this.checkBoxPrice.UseVisualStyleBackColor = true;
            // 
            // checkBoxRoom
            // 
            this.checkBoxRoom.AutoSize = true;
            this.checkBoxRoom.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.checkBoxRoom.Location = new System.Drawing.Point(323, 73);
            this.checkBoxRoom.Name = "checkBoxRoom";
            this.checkBoxRoom.Size = new System.Drawing.Size(80, 19);
            this.checkBoxRoom.TabIndex = 17;
            this.checkBoxRoom.Text = "Комнаты";
            this.checkBoxRoom.UseVisualStyleBackColor = true;
            // 
            // comboBoxRegion
            // 
            this.comboBoxRegion.FormattingEnabled = true;
            this.comboBoxRegion.Location = new System.Drawing.Point(148, 72);
            this.comboBoxRegion.Name = "comboBoxRegion";
            this.comboBoxRegion.Size = new System.Drawing.Size(163, 21);
            this.comboBoxRegion.TabIndex = 19;
            // 
            // numericUpDownRoomAmount
            // 
            this.numericUpDownRoomAmount.Location = new System.Drawing.Point(404, 73);
            this.numericUpDownRoomAmount.Name = "numericUpDownRoomAmount";
            this.numericUpDownRoomAmount.Size = new System.Drawing.Size(94, 20);
            this.numericUpDownRoomAmount.TabIndex = 20;
            this.numericUpDownRoomAmount.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // FormProperty
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(748, 450);
            this.Controls.Add(this.numericUpDownRoomAmount);
            this.Controls.Add(this.comboBoxRegion);
            this.Controls.Add(this.checkBoxRoom);
            this.Controls.Add(this.checkBoxPrice);
            this.Controls.Add(this.checkBoxRegion);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.textBoxPriceTo);
            this.Controls.Add(this.textBoxPriceFrom);
            this.Controls.Add(this.dataGridViewObjects);
            this.Controls.Add(this.buttonAddON);
            this.Controls.Add(this.buttonEditON);
            this.Controls.Add(this.buttonInfoON);
            this.Controls.Add(this.buttonInfoOwner);
            this.Controls.Add(this.buttonReset);
            this.Controls.Add(this.buttonSearch);
            this.Controls.Add(this.buttonBack);
            this.Name = "FormProperty";
            this.Text = "FormProperty";
            this.Load += new System.EventHandler(this.FormProperty_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewObjects)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownRoomAmount)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonBack;
        private System.Windows.Forms.Button buttonSearch;
        private System.Windows.Forms.Button buttonReset;
        private System.Windows.Forms.Button buttonInfoOwner;
        private System.Windows.Forms.Button buttonInfoON;
        private System.Windows.Forms.Button buttonEditON;
        private System.Windows.Forms.Button buttonAddON;
        private System.Windows.Forms.DataGridView dataGridViewObjects;
        private System.Windows.Forms.TextBox textBoxPriceFrom;
        private System.Windows.Forms.TextBox textBoxPriceTo;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox checkBoxRegion;
        private System.Windows.Forms.CheckBox checkBoxPrice;
        private System.Windows.Forms.CheckBox checkBoxRoom;
        private System.Windows.Forms.ComboBox comboBoxRegion;
        private System.Windows.Forms.DataGridViewTextBoxColumn Владелец;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.NumericUpDown numericUpDownRoomAmount;
    }
}