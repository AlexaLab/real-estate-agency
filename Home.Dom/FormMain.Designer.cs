﻿namespace Home.Dom {
    partial class FormMain {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
            this.buttonAboutAgency = new System.Windows.Forms.Button();
            this.buttonAboutObject = new System.Windows.Forms.Button();
            this.buttonAboutAgent = new System.Windows.Forms.Button();
            this.buttonAutorize = new System.Windows.Forms.Button();
            this.buttonExit = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonAboutAgency
            // 
            this.buttonAboutAgency.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.buttonAboutAgency.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonAboutAgency.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonAboutAgency.Location = new System.Drawing.Point(81, 12);
            this.buttonAboutAgency.Name = "buttonAboutAgency";
            this.buttonAboutAgency.Size = new System.Drawing.Size(272, 63);
            this.buttonAboutAgency.TabIndex = 0;
            this.buttonAboutAgency.Text = "Об агенстве недвижимости";
            this.buttonAboutAgency.UseVisualStyleBackColor = false;
            this.buttonAboutAgency.Click += new System.EventHandler(this.buttonAboutAgency_Click);
            // 
            // buttonAboutObject
            // 
            this.buttonAboutObject.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.buttonAboutObject.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonAboutObject.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonAboutObject.Location = new System.Drawing.Point(12, 81);
            this.buttonAboutObject.Name = "buttonAboutObject";
            this.buttonAboutObject.Size = new System.Drawing.Size(341, 63);
            this.buttonAboutObject.TabIndex = 1;
            this.buttonAboutObject.Text = "Об объектах недвижимости";
            this.buttonAboutObject.UseVisualStyleBackColor = false;
            this.buttonAboutObject.Click += new System.EventHandler(this.buttonAboutObject_Click);
            // 
            // buttonAboutAgent
            // 
            this.buttonAboutAgent.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.buttonAboutAgent.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonAboutAgent.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonAboutAgent.Location = new System.Drawing.Point(12, 150);
            this.buttonAboutAgent.Name = "buttonAboutAgent";
            this.buttonAboutAgent.Size = new System.Drawing.Size(341, 63);
            this.buttonAboutAgent.TabIndex = 2;
            this.buttonAboutAgent.Text = "Об агентах-риелторах";
            this.buttonAboutAgent.UseVisualStyleBackColor = false;
            // 
            // buttonAutorize
            // 
            this.buttonAutorize.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.buttonAutorize.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonAutorize.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonAutorize.Location = new System.Drawing.Point(12, 219);
            this.buttonAutorize.Name = "buttonAutorize";
            this.buttonAutorize.Size = new System.Drawing.Size(341, 63);
            this.buttonAutorize.TabIndex = 3;
            this.buttonAutorize.Text = "Авторизация";
            this.buttonAutorize.UseVisualStyleBackColor = false;
            this.buttonAutorize.Click += new System.EventHandler(this.buttonAutorize_Click);
            // 
            // buttonExit
            // 
            this.buttonExit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.buttonExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonExit.Location = new System.Drawing.Point(12, 288);
            this.buttonExit.Name = "buttonExit";
            this.buttonExit.Size = new System.Drawing.Size(341, 63);
            this.buttonExit.TabIndex = 4;
            this.buttonExit.Text = "Выход";
            this.buttonExit.UseVisualStyleBackColor = false;
            this.buttonExit.Click += new System.EventHandler(this.buttonExit_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.BackgroundImage = global::Home.Dom.Properties.Resources.Logo;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox1.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.InitialImage")));
            this.pictureBox1.Location = new System.Drawing.Point(13, 13);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(62, 62);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 5;
            this.pictureBox1.TabStop = false;
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(365, 369);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.buttonExit);
            this.Controls.Add(this.buttonAutorize);
            this.Controls.Add(this.buttonAboutAgent);
            this.Controls.Add(this.buttonAboutObject);
            this.Controls.Add(this.buttonAboutAgency);
            this.Name = "FormMain";
            this.Text = "Home.Dom";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonAboutAgency;
        private System.Windows.Forms.Button buttonAboutObject;
        private System.Windows.Forms.Button buttonAboutAgent;
        private System.Windows.Forms.Button buttonAutorize;
        private System.Windows.Forms.Button buttonExit;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}

