﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Home.Dom
{
    public partial class FormAutorize : Form
    {
        public FormAutorize()
        {
            InitializeComponent();
        }

        private void buttonLogin_Click(object sender, EventArgs e)
        {
            //связь с БД
            SqlConnection connection = new SqlConnection();
            connection.ConnectionString = ClassTotal.connectionString;
            connection.Open();

            string log = this.textBoxLog.Text;
            string pass = this.textBoxPass.Text;

            SqlCommand command = new SqlCommand();
            command.CommandText = $"SELECT * FROM [Users] WHERE[Login] = '{log}' AND[Password] = '{pass}'";
            command.Connection = connection;

            SqlDataReader reader = command.ExecuteReader();

            if (reader.Read() == true)
            {
                ClassTotal.ID_User = (int)reader["ID_User"];
                ClassTotal.ID_Role = (int)reader["ID_Role"];                

                switch (ClassTotal.ID_Role)
                {
                    case 1: MessageBox.Show("Вы зашли как директор");
                        this.Close();
                        FormDirector fd = new FormDirector();
                        fd.ShowDialog();
                        break;

                    case 2: MessageBox.Show("Вы зашли как администратор");
                        this.Close();
                        FormAdmin fa = new FormAdmin();
                        fa.ShowDialog();
                        break;

                    case 3: MessageBox.Show("Вы зашли как агент");
                        this.Close();
                        FormAgentAndON AgentAndON = new FormAgentAndON();
                        //FormAgent fag = new FormAgent();
                        AgentAndON.ShowDialog();
                        break;
                }
            }
            else
            {
                MessageBox.Show("Неверное имя пользователя или пароль");
            }
        }

        private void buttonBack_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
