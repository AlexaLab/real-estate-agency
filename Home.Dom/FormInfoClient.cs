﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Net.Mail;
using System.Net;

namespace Home.Dom
{
    public partial class FormInfoClient : Form
    {

        int mode1;
        int idAgent1;

        public FormInfoClient()
        {
            InitializeComponent();
            mode1 = 3;
            this.textBoxIDClient.Text = "";
            this.textBoxNameClient.Text = "";
            this.maskedTextBoxPhoneClient.Text = "";
            this.textBoxEmailClient.Text = "";
            this.textBoxAddressClient.Text = "";
            this.textBoxPassportClient.Text = "";
            this.textBoxInnClient.Text = "";
            this.textBoxSnilsClient.Text = "";
            this.textBoxBirthdayClient.Text = "";
            this.textBoxRegistrationClient.Text = "";
        }

        SqlConnection connection;
        SqlCommand command;

        public FormInfoClient(int mode1, int idAgent1)
        {
            InitializeComponent();

            this.mode1 = mode1;
            this.idAgent1 = idAgent1;

            connection = new SqlConnection();
            connection.ConnectionString = ClassTotal.connectionString;
            connection.Open();

            command = connection.CreateCommand();
            command.CommandText = $"SELECT * FROM [Clients] WHERE ID_agent = {ClassTotal.ID_User} AND [ID_client] = {idAgent1}";

            try
            {
                SqlDataReader dataReader = command.ExecuteReader();
                dataReader.Read();

                this.textBoxIDClient.Text = dataReader["ID_client"].ToString();
                this.textBoxNameClient.Text = dataReader["Name_client"].ToString();
                this.maskedTextBoxPhoneClient.Text = dataReader["Phone_client"].ToString();
                this.textBoxEmailClient.Text = dataReader["Email"].ToString();
                this.textBoxAddressClient.Text = dataReader["Address_client"].ToString();
                this.textBoxPassportClient.Text = dataReader["Password"].ToString();
                this.textBoxInnClient.Text = dataReader["INN"].ToString();
                this.textBoxSnilsClient.Text = dataReader["CNULC"].ToString();
                this.textBoxBirthdayClient.Text = dataReader["Date_birth"].ToString();
                this.textBoxRegistrationClient.Text = dataReader["Date_registration"].ToString();

                dataReader.Close();
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                Application.Exit();
            }
        }

        private void buttonBackClient_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonSaveClient_Click(object sender, EventArgs e)
        {

            connection = new SqlConnection();
            connection.ConnectionString = ClassTotal.connectionString;
            connection.Open();
            SqlCommand command = connection.CreateCommand();
            DataTable dt = new DataTable();

            try
            {
                switch (mode1)
                {
                    case 1:
                        break;
                    case 2:
                        break;
                    case 3:
                        SqlDataAdapter adapter = new SqlDataAdapter("SELECT * FROM [Clients]", ClassTotal.connectionString);
                        string StrSql = $"INSERT INTO [Clients] ([Name_client], [Email], [Phone_client], [Password], [Address_client], [Date_birth], [INN], [CNULC], [Date_registration], [ID_agent])" +
                        $"VALUES ('{Convert.ToString(textBoxNameClient.Text)}', '{Convert.ToString(textBoxEmailClient.Text)}', '{Convert.ToString(maskedTextBoxPhoneClient.Text)}', " +
                        $"'{Convert.ToString(textBoxPassportClient.Text)}', '{Convert.ToString(textBoxAddressClient.Text)}', '{Convert.ToDateTime(textBoxBirthdayClient.Text)}', '{Convert.ToString(textBoxInnClient.Text)}', " +
                        $"'{Convert.ToString(textBoxSnilsClient.Text)}', '{Convert.ToDateTime(textBoxRegistrationClient.Text)}', {ClassTotal.ID_User})";
                        command.CommandText = StrSql;
                        adapter.InsertCommand = command;
                        adapter.Update(dt);

                        int result = adapter.InsertCommand.ExecuteNonQuery();
                        MessageBox.Show($"Добавлена {result} строка");
                        this.Close();
                        break;
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show("Не удалось добавить новые данные");
                MessageBox.Show(ex.Message);
                return;
            }
        }

        private void FormInfoClient_Load(object sender, EventArgs e)
        {
            if (mode1 == 2 || mode1 == 3)
            {
                textBoxIDClient.Visible = false;
                label1.Visible = false;
            }
        }
    }
}

